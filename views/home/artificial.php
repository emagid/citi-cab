<video type="video/mov" autoplay muted loop src='<?= FRONT_ASSETS ?>img/back.mov' class='background'></video>

<div class='content'>    
<section class='inner_page page'>
    <p class='banner' style='background-color: #00bdf2'>Artificial Intelligence</p>
    <p class='close'>BACK</p>
    <img class='img_line' src="<?=FRONT_ASSETS?>img/menu_line.png">

    <div class='links'>
        <p>In this evolving business ecosystem, <strong>data is king.</strong></p>
        <ul>
            <li style='color: #c99700'><span><strong>Advancements in analytics such as machine learning can give companies a significant advantage</strong> through data-driven decision-making, targeted marketing and problem-solving that addresses the biggest challenges and pain points businesses face</span></li>
            <li style='color: #c99700'><span>Beyond developing virtual assistant/chat bot recommendation engines that are fully automated, <strong>Citi’s goal is to move completely to automation and digitization</strong></span></li>
        </ul>
    </div>
</section>
</div>
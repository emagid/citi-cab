<video type="video/mov" autoplay muted loop src='<?= FRONT_ASSETS ?>img/back.mov' class='background'></video>

<div class='content'>    
<section class='inner_page page'>
    <p class='banner' style='background-color: #84bd00'>Connectivity</p>
    <p class='close'>BACK</p>
    <img class='img_line' src="<?=FRONT_ASSETS?>img/menu_line.png">

    <div class='links'>
        <p><strong>As hyper-connectivity becomes the norm, corporate banking APIs are expected to proliferate rapidly, enabling banks, Fintechs and corporates to readily combine services.</strong></p>
        <ul>
            <li style='color: #84bd00'><span><strong>These potential benefits may range from the convenience of real-time access to bank services directly from applications;</strong> cost reductions made possible by cutting out any middle-men; reduced risk resulting from having fewer parties involved in transactions; improved reconciliation facilitated by direct access to account data; and a shift from reactive to proactive functions that leverage dynamically exchanged workflow data based on events, patterns or time</span></li>
            <li style='color: #84bd00'><span><strong>The future of the digital world may lie in platforms that connect to each other through multiple API services,</strong> creating opportunities for greater automation, efficiency and a superior customer experience</span></li>
            <li style='color: #84bd00'><span><strong>Corporates can use APIs to build advisory services within their treasury application</strong> that can automatically and proactively alert the user of payment status based on information directly queried from the bank</span></li>
        </ul>
    </div>
</section>
</div>
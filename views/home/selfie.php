<video type="video/mov" autoplay muted loop src='<?= FRONT_ASSETS ?>img/back.mov' class='background'></video>

<div class='content'>    
    <p class='banner' style='background-color: #6b3077'>Selfie</p>
    <section class="photobooth" >
            <section id='photos' class='photos'>
                <!-- Cam -->
                <div class='take_photo'>
                    <h3 id='takePic' class='pic_text'><span>Press </span> to shoot</h3>
                    <video id="video" width="1080px" height="1700px" autoplay></video>
                    <device type="media" onchange="update(this.data)"></device>
                    <script>
                        function update(stream) {
                            document.querySelector('video').src = stream.url;
                        }
                    </script>
                </div>

                <div class='countdowner'>
                   <div class='countdown'>3</div>
                   <div class='countdown'>2</div>
                   <div class='countdown'>1</div>
                </div>
            </section>

            <div class='flash'></div>


            <!-- Choosing pictures -->
            <div id='pictures'>
                <div class='button next'>NEXT</div>
                <!-- <p class='gif_info'>Choose 4 photos to create your GIF</p> -->
                <div class='button submit'>SHARE</div>
               <div class='button retake'>RETAKE</div>
                <div class='container gif_container'></div>
            </div>

            <!-- Showing Gif -->
           <!--  <section class='gif_show'>
                <img id='loader' src="<?=FRONT_ASSETS?>img/loader.gif">
                <div class='button submit'>SHARE</div>
            </section> -->

            <!-- Share overlay -->
            <section class='share_overlay'>
                <div class='container'>
                    <form id='submit_form'>
                        <input type='hidden' name='form' value="1">
                        <input type="hidden" name="image" class="image_encoded">
                        <span>
                            <input class='input jQKeyboard first_email' name='email[]' pattern="[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,3}$" type='text' placeholder='Email' title='Please enter a valid email address.'>
                        </span>
                        <p id='add_email' style='color: #ffffffab;'>Add an email +</p>
                        <div class='line'></div>
                        <span>
                            <input class='input donate_key donateinput first_phone' name="phone[]" placeholder="Phone Number (eg: +19874563210)">
                        </span>
                        <p id='add_phone' style='color: #ffffffab;'>Add a phone number +</p>
                        <input id='share_btn' class='button' type='submit' value='SHARE'>
                        <!-- <div class='button print'>PRINT</div> -->
                    </form>
                    <!-- <a href='/' id='done' class='button'>DONE</a> -->
                </div>
            </section>

            <!-- Alerts -->
            <section id='share_alert'>
                <h3>Thank you!</h3>
                <p>Your picture is on its way</p>
            </section>

            <section class='event_pics'>

                 <? foreach ($model->event_pictures as $event_picture) { $img_path = UPLOAD_URL.'Snapshots'.DS.$event_picture->image;
                    ?>
                    <img src="<?php echo $img_path ?>">  
                <? } ?>
            </section>

            <div class='popup'>
                <div class='off_click'></div>
                <p class='close'>X</p>
            </div>
      </section>



      <script type="text/javascript">
    var keyboard;
            $(function(){
                keyboard = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                            [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['-', '-', 189, 0, false], ['=', '=', 187, 0, false],
                                ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                                ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                                ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                                ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                                ['Shift', '16', 16, 2, true], ['z', 'z', 90, 0, false], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                                ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], ['Shift', '16', 16, 2, false],
                                ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                            ]
                        ]
                    ]
                }
                $('input.jQKeyboard').initKeypad({'keyboardLayout': keyboard});


                var board = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                        ]
                    ]
                }
                $('input.donate_key').initKeypad({'donateKeyboardLayout': board});
            });
      </script>


      <script type="text/javascript">
            $(document).ready(function(){
                var images = <?= \Model\Snapshot_Contact::slider()?>; //array of image urls
                var gifs =   <?= \Model\Gif::slider()?>; //array of gif urls

                slideStart();
                addMedia(images, gifs);

                function addMedia(images, gifs) {
                  for ( i=0; i<images.length; i++ ) {
                    var div = "<img class='event_pic' src='" + images[i].replace('\\','/') + "'></div>";
                    $('.event_pics').append(div);
                  }
                      
                  for ( i=0; i<gifs.length; i++ ) {
                        var div = "<img class='event_pic' src='" + gifs[i].replace('\\','/') + "'></div>";
                      $('.event_pics').append(div);
                  }
                }

                $('#add_email').click(function(){
                    $('.first_email').parent().append("<input required class='input jQKeyboard' name='email[]' pattern='[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,3}$' type='text' placeholder='Email' title='Please enter a valid email address.'>");
                    $('.first_email').parent().find('input').last().initKeypad({'keyboardLayout': keyboard});
                });

                $('#add_phone').click(function(){
                    $('.first_phone').parent().append("<input required class='input jQKeyboard' name='phone[]' placeholder='Phone Number (eg: +19874563210)'>");
                    $('.first_phone').parent().find('input').last().initKeypad({'keyboardLayout': keyboard});
                });



                // $('.event_pics')
                //     .on('init', function(slick) {
                //         $('.slickSlider').css("overflow","visible");
                //         $('.event_pics').addClass('slide_active');
                //     })
                // .slick({
                //     slidesToShow: 2,
                //     autoplay: true,
                //     focusOnSelect: false,
                //     lazyLoad: 'ondemand',
                //     speed: 1000
                // });



                $('.event_pics').slick({
                    dots: false,
                    slidesToShow: 2,
                    autoplay: true,
                    autoplaySpeed: 1000
                });
            });
        </script>



        <script type="text/javascript">
            $(document).ready(function(){
        // ===========  CAMERA FUNCTION  ==============

            // Grab elements, create settings, etc.
            var video = document.getElementById('video');

            // Get access to the camera!
            if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                // Not adding `{ audio: true }` since we only want video now
                navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                    video.src = window.URL.createObjectURL(stream);
                    video.play();
                });
            }

            $('.retake').click(function(){
                $('#pictures').fadeOut();
                $('#photos, #takePic, .event_pics').fadeIn();
                $('.pic_text').css('pointer-events', 'all');
            });

            $('.event_pics img').click(function(){
                $('.popup').append($(this).clone());
                $('.popup').fadeIn();
                $('.popup').css('display', 'flex');
            });

            $('.off_click, .close').click(function(){
                $('.popup').fadeOut(500);
                $('.event_pics').slick('slickPlay');
                setTimeout(function(){
                    $('.popup img').remove();
                }, 500);
            });

         
            function countDown() {
                $('.countdowner').fadeIn();
                var divs = $('.countdown');
                var timer
                var offset = 0 

                divs.each(function(){
                    var self = this;
                    timer = setTimeout(function(){
                        $(self).show();
                        $(self).css('font-size', '150px');
                        $(self).fadeOut(1000);
                    }, 1000 + offset);
                    offset += 1000;
                });

                // flash
                setTimeout(function(){
                    $('.flash').fadeIn(200);
                    $('.flash').fadeOut(400);
                    $(divs).css('font-size', '0px');
                }, 4000);
            }


            function gifPhotos() {
                var divs = $('.countdown');
                var offset = 1500

                for ( i=0; i<5; i++) {

                    (function(i){
                      setTimeout(function(){
                            $('#pictures .gif_container').append("<div class='canvas_holder gif'><canvas class='canvas' width='900' height='675'></canvas></div>")
                            var canvas = $('.canvas');
                            var context = canvas[canvas.length -1].getContext('2d');
                            var video = document.getElementById('video');


                             $('.flash').fadeIn(200);
                            $('.flash').fadeOut(400);

                            $(divs).css('font-size', '0px');

                           context.drawImage(video, 0, 0, 900, 675);
                    
                           $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).css('display', 'inline-block');

                           // watermark
                            // var img2 = new Image();
                            // img2.src = '/content/frontend/assets/img/logo_holder.png';
                            // context.drawImage(img2,530,420, 80, 43);

                            $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))
                      
                      }, offset);
                      offset += 1500
                    }(i));  
                }
            }


            function showGifs() {
                var photos = $('#pictures').children('.canvas_holder');
                $('#photos').hide();
                $('#pictures').fadeIn(1000);
                $('.gif_info').fadeIn();
                $('.home').css('position', 'relative');
                // debugger
                for ( i=0; i<6; i++ ) {
                    $(photos[i]).css('display', 'inline-block');
                }
            }


            function convertCanvasToImage(canvas) {
                var image = new Image();
                image.src = canvas.toDataURL("image/png");
                console.log(image)
                return image;
            }

            function convertCanvasToImage2(canvas) {
                var dataURL = canvas.toDataURL();
                return canvas.src = dataURL;
            }

            


            function showPictures() {
                $('#pictures').fadeIn(2000);
                var photos = $('#pictures').children('.photo');
                    $(photos[0]).fadeIn();

                $.post('/contact/save_img/',{image:$($(photos[0]).html()).attr('src')},function(data){
                    $('#submit_form input[name=image]').val(data.image.id);
                });

                $('#pen').fadeIn();

            }

            $('#submit_form').on('submit', function(e){
                e.preventDefault();
                $('#share_alert').slideDown();
                $('#share_alert').css('display', 'flex');
                $.post('/home/selfie', $(this).serialize(), function (response) {
                    if(response.status){
                        $('#submit_form')[0].reset();

                        setTimeout(function(){
                            $('#share_alert').slideUp();
                            window.location.href = '/';
                        }, 3000);
                    }
                });
            });



            // GIF CLICK
            // document.getElementById("snap_gif").addEventListener("click", function() {
            //     $('.camera_button').fadeOut(1000);
            //     $('.camera_button').css('pointer-events', 'none');
            //     var pics = []

            //     countDown();
            //     setTimeout(function(){
            //         $('#pictures .gif_container').append("<div class='canvas_holder gif'><canvas class='canvas' width='900' height='675'></canvas></div>")                
            //             var canvas = $('.canvas');
            //             var context = canvas[canvas.length -1].getContext('2d');
            //             var video = document.getElementById('video');
            //             var background = $('#background')[0];
            //             var logo = $('#logo')[0];
            //            context.drawImage(background, 0, 0, 900, 675);
            //            context.drawImage(video, 0, 0, 900, 675);
            //            context.drawImage(logo, 190, 850, 520, 160);
                       

            //            $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).css('display', 'inline-block');
            //            $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))


            //        gifPhotos();

            //     }, 4000 )

            //     setTimeout(function(){
            //         $('#photos').css('opacity', '0');
            //         setTimeout(function(){
            //             $('#photos').fadeOut();
            //         }, 1100);

            //         showGifs();
            //         $(this).css('pointer-events', 'all');
            //     }, 10000);
            // });



            // PHOTO CLICK
            document.getElementById("takePic").addEventListener("click", function() {
                $('.pic_text, .event_pics').fadeOut(1000);
                $('.pic_text').css('pointer-events', 'none');
                var pics = []

                countDown();
                    // take the picture
                    setTimeout(function(){
                        $('#pictures').append("<div class='canvas_holder photo'><canvas id='pic' class='canvas' width='900' height='675'></canvas></div>")
                        
                        var canvas = $('#pic');
                        var context = canvas[canvas.length -1].getContext('2d');
                        var video = document.getElementById('video');

                       context.drawImage(video, 0, 0, 900, 675);
                       

                        $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))


                        $('#pictures').append("<canvas id='draw' class='drawing' width='900' height='675'></canvas>");
                        var drawcanvas = $('#draw');
                        var ctx = drawcanvas[0].getContext('2d');
                        var video = document.getElementById('video');

                       ctx.drawImage(video, 0, 0, 900, 675);

                    }, 4000);

                setTimeout(function(){
                    $('#photos').slideUp()
                    $('.submit, .retake').delay(1000).fadeIn();
                    $('.print').delay(1000).fadeIn();
                    $(this).css('pointer-events', 'all');
                    showPictures();
                }, 5000);
            });

        });
    </script>

</div>




<video type="video/mov" autoplay muted loop src='<?= FRONT_ASSETS ?>img/back.mov' class='background'></video>
<div class='content'>    
    <section id='home' class='homepage page'>
        <div class='welcome'>
            <p><strong>Welcome</strong> to</p>
            <p class="date">20</p>
            <p class="date">18</p>
            <p>Fall Treasury and Trade Solutions Client Advisory Board</p>
        </div>

        <div class='videos'>
            <video class='active_vid' muted autoplay >
                <source src="<?= FRONT_ASSETS ?>img/vid1.mp4" type="video/mp4">
            </video>
            <video muted autoplay >
                <source src="<?= FRONT_ASSETS ?>img/vid2.mp4" type="video/mp4">
            </video>
            <video muted autoplay >
                <source src="<?= FRONT_ASSETS ?>img/vid3.mp4" type="video/mp4">
            </video>
            <video class='last_vid' muted autoplay >
                <source src="<?= FRONT_ASSETS ?>img/vid4.mp4" type="video/mp4">
            </video>
        </div>
    </section>
</div>    

<script type="text/javascript">
    // $('.active_vid').ended(function(){
    //     alert('hi')
    // });

    $('video').on('ended',function(){
        if ( $(this).hasClass('active_vid') ) {
            if ( $(this).hasClass('last_vid') ) {
                $(this).fadeOut();
                $('video').removeClass('active_vid');
                $($('.videos video')[0]).addClass('active_vid');
                $($('.videos video')[0]).fadeIn();
                $('.videos video')[0].currentTime = 0;
            } else {
              $(this).fadeOut();
              $(this).removeClass('active_vid');
              $(this).next('video').fadeIn();
              $(this).next('video')[0].currentTime = 0;
              $(this).next('video').addClass('active_vid');
            }
        }
    });
</script>

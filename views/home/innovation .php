<video type="video/mov" autoplay muted loop src='<?= FRONT_ASSETS ?>img/back.mov' class='background'></video>

<div class='content'>    
<section class='inner_page page'>
    <p class='banner' style='background-color: #ed8b00'>Innovation Defined</p>
    <p class='close'>BACK</p>
    <img class='img_line' src="<?=FRONT_ASSETS?>img/menu_line.png">

    <div class='links'>
        <ul>
            <li style='color: #ed8b00'><span><strong>Creating client value</strong>, directly and indirectly, through the application and delivery of new ideas</span></li>
            <li style='color: #ed8b00'><span><strong>Clients are at the center of what we do and they are at the core of our innovation ecosystem.</strong> Being the best for our clients is embedded into the fabric of how we think about innovation</span></li>
        </ul>
    </div>
</section>
</div>
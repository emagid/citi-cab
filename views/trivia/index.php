<main>
    <? $questions = $model->questions?>
	<section class="trivia_page" >

        <!-- Header -->
        <div class='vid_overlay'></div>     
        <div class='vid' style='z-index: -1;'></div>
        <header>
            <a href="/"><img src="<?=FRONT_ASSETS?>img/webair.png"></a>
        </header>

        <!-- trivia questions -->
          <section class="trivia_content trivia">
            <div class="title_holder">
              <p>1 / <?=count($questions)?></p>
            </div>

            <!-- question 1 -->
            <?php foreach ($questions as $i => $question) {?>
                <div class="question_holder" data-fail_text="<?=$question->failure_text?>">
                    <div class="question">
                        <h2>QUESTION <?=$i+1?></h2>
                        <p><?=$question->text?></p>
                        <? if ($question->image != null && $question->image != '') {?>
                            <img src="<?=UPLOAD_URL . 'questions/' . $question->image ?>" width="100"/>
                        <? } ?>
                    </div>

                    <? $choices = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' ?>

                    <?foreach ($question->answers as $a => $answer) {?>
                    <div class="answer click_action <?=$question->correct_answer_id == $answer->id ? 'correct_answer' : ''?>">
                        <h2><?=substr($choices,$a,1)?></h2>
                        <p><?=$answer->text?></p>
                        <? if ($answer->featured_image != null && $answer->featured_image != '') {?>
                            <img src="<?=UPLOAD_URL . 'answers/' . $answer->featured_image ?>" width="100"/>
                        <? } ?>
                    </div>
                    <? } ?>
                    <button class="button submit">NEXT</button>
                </div>
            <? } ?>
          </section>
            <section class="trivia_completion trivia">

              <div class="question">
                <h2></h2>
                <p>Thanks for Playing</p>
              </div>


            </section>
        </section>  

    <div class="missing">
      <h2>Name and Email both are required</h2>
    </div>

    <div class='complete'>
      <h2>You've been entered!</h2>
    </div>

    <div class='incomplete'>
      <h2>You already submitted your entry</h2>
    </div>

</main>



<script>
$(document).ready(function () {
      $('.trivia_content').fadeIn('fast');
      $('.trivia_content').css('padding-top', '0px');


          let amount_right = 0
        $(document).on('click', '.answer', function(){

            let failure_text = $(this).parents('.question_holder').data('fail_text');
            
            // if correct answer
            if($(this).hasClass('correct_answer')){ 
              amount_right ++

              // remove ability to choose another answer
                $(this).parents('.question_holder').children('.answer').each(function(){
                    $(this).removeClass('answer click_action');
                    $(this).addClass('answer_outcome');
                });

                var answers = $(this).parent('.question_holder').children('.answer_outcome');
                for ( i=0; i<answers.length; i++ ) {
                  if ( !$(answers[i]).hasClass('correct_answer') ) {
                    $(answers[i]).css('opacity', '0');
                    $(answers[i]).delay(2000).slideUp();
                  }
                }
              
              $(this).after("<div class='answer_outcome correct'> <h2 class='right_answer'>CORRECT</h2></div>");
              $('.correct').slideDown();
              // if ( failure_text !== "" ) {
              //   $(this).after("<div class='answer_outcome correct_info'></div>");
              //   $('.correct_info').append("<p>" + failure_text + "</p>");
              //   $('.correct_info').delay(2500).slideDown();
              // }

              $(this).parent('.question_holder').children('.submit').delay(2500).fadeIn('fast');
       

            // Wrong answer
            } else { 

                // remove ability to choose another answer
                $(this).parents('.question_holder').children('.answer').each(function(){
                    $(this).removeClass('answer click_action');
                    $(this).addClass('answer_outcome');
                });

                var answers = $(this).parent('.question_holder').children('.answer_outcome');
                for ( i=0; i<answers.length; i++ ) {
                  if ( !$(answers[i]).hasClass('correct_answer') ) {
                    $(answers[i]).css('opacity', '0');
                    $(answers[i]).delay(2000).slideUp();
                  }
                }

                $(this).after("<div class='answer_outcome wrong'> <h2 class='right_answer'>INCORRECT</h2></div>");
                $('.wrong').slideDown();
                    $('.wrong').css('opacity', '0');
                    $('.wrong').delay(2000).slideUp();
                // if ( failure_text !== "" ) {
                //   $(this).parents('.question_holder').children('.correct_answer').after("<div class='answer_outcome correct_info'></div>");
                //   $('.correct_info').append("<p>" + failure_text + "</p>");
                //   $('.correct_info').delay(2500).slideDown();
                // }

                $(this).parent('.question_holder').children('.submit').delay(2500).fadeIn('fast');
            }

        });


          // Next questions
          var timer;
          var question_num = 1
          var total_questions = $('.title_holder p').html().substring($('.title_holder p').html().length - 2);
          $(".submit").on({
               'click': function clickAction() {
                   var self = this;
                   $(self).css('pointer-events', 'none');
                   if ( question_num === parseInt(total_questions)) {
                      $('.trivia_content').css('padding-top', '200px');
                      $('.trivia_content').fadeOut('fast');
                   }else {
                      $(self).parent('.question_holder').css('margin-top', '200px');
                      $(self).parent('.question_holder').fadeOut('slow');
                   }

                   parseInt(total_questions)

                    timer = setTimeout(function () {
                      if ( question_num === parseInt(total_questions)) {
                        $('.trivia_completion').fadeIn('slow');
                        $('.trivia_completion').css('padding-top', '0px');
                        $('.trivia_completion .question h2').html(amount_right + " / " + total_questions);

                        setTimeout(function(){
                          $('.white').fadeIn();
                          setTimeout(function(){
                            window.location = '/';
                          }, 700);
                        }, 10000);
                      }else {

                        if ( question_num === 3 ) {
                          $(self).parent('.question_holder').next().children('.answer').addClass('correct_answer');
                        }

                        $(self).parent('.question_holder').next().fadeIn('slow');
                        $(self).parent('.question_holder').next().css('margin-top', '0px');

                        // Updating question number
                        $('.title_holder p').html((question_num += 1).toString() + "  / " + total_questions )
                      }
                      $(self).css('pointer-events', 'all');

                    }, 1000);
               }
          });

          // $('.entry-form').on("click", function(){
          //   var email = $('#email_input').val();
          //   console.log("This is " + email);
          //   $.post('/trivia/trivia_entry',{email:email},function(data){
          //     console.log(data);
          //   });
          // })

         // Go home after trivia completion
         $("#email input[type='submit']").on('click', function(e){
            e.preventDefault();
            var email = $('#email_input').val();
            var name = $('#name_input').val();
            if(email != '' && name != ''){
                $.post('/trivia/trivia_entry',$('#email').serialize(),function(data){
                  if(data['status'] == true){
                    $('.complete').fadeIn();
                    $('.complete').css('display', 'flex');
                    setTimeout(function(){
                      $('.white').fadeIn();
                        setTimeout(function(){
                            $('#email').submit();
                            window.location = '/';
                        }, 700);
                    }, 3000);
                  }
                  else{
                    $('.incomplete').fadeIn();
                    $('.incomplete').css('display', 'flex');
                    setTimeout(function(){
                      $('.white').fadeIn();
                        setTimeout(function(){
                            $('#email').submit();
                        }, 700);
                    }, 3000);
                  }
                });
            } else {
                $('.missing').fadeIn();
            }
         });
         $('#email').click(function(){
          $('div.jQKeyboardContainer').css('top','785px');
         });
         
    });

</script>




 
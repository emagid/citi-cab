<?php

class surveysController extends adminController {
	
	function __construct(){
		parent::__construct("Survey");
	}
	
	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;		

		parent::index($params);
	}

	function update(Array $arr = []){
				
		parent::update($arr);
	}

	function update_post(){
				
		parent::update_post();
	}

	public function exportSurveys(Array $params = []){
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=citi_cab_event_survey.csv');
        
        $sql="SELECT * FROM question WHERE active=1 order by display_order";

        $surveys = \Model\Question::getList(['sql'=>$sql]);
        $output = fopen('php://output', 'w');
        $t=array("No.",'Question','Survey Answers','Total Counts');
        fputcsv($output, $t);
        $row ='';

        foreach($surveys as $key=>$survey) {
        	$qanswers = \Model\Answer::getlist(['where'=>"question_id = ".$survey->id]);
        	if($survey->display_order == 6){
        		$survey_answers = \Model\Survey::getList(['where'=>"question = '".$survey->id."'"]);
        	 	foreach($survey_answers as $sa){
        	 		$row = array($key+1,$survey->text,$sa->answer,'');
        	 		fputcsv($output, $row);
        	 	}
        	} else {
	        	foreach ($qanswers as $key => $answer) {
	        	 	$answers_count = \Model\Survey::getCount(['where'=>"question = '".$survey->id."' AND answer = '".$answer->id."'"]);
	        	 	$row = array($key+1,$survey->text,$answer->text,$answers_count);
	        	 	fputcsv($output, $row);
	        	}
	        } 
            // $row = array($key+1,$survey->question,$survey->answer);
            // fputcsv($output, $row);  
        }
    }
  
}
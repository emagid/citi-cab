<div class='white'>
	<img class='loader' src="<?=FRONT_ASSETS?>img/loader.gif">
</div>

<header>
	<img class='cab' src="<?=FRONT_ASSETS?>img/cab.png">
	<p class='menu'>MENU</p>
</header>

<section class='main_menu'>
	<p class='banner'>MAIN MENU</p>
	<p class='close'>BACK</p>
	<img class='img_line' src="<?=FRONT_ASSETS?>img/menu_line.png">

	<div class='links'>
		<a id='cyber' class='#c99700'>CyberSecurity</a>
		<a id='artificial' class="#00bdf2">Artificial Intelligence</a>
		<a id='blockchain' class="#00b0b9">Blockchain</a>
		<a id='connectivity' class="#84bd00">Connectivity</a>
		<a id='onboarding' class="#c4d600">Digital Onboarding</a>
		<a id='experience' class="#ffcd00">Client Experience</a>
		<a id='innovation' class="#ed8b00">Innovation Defined</a>
		<a id='survey' class="#c6007e">Survey</a>
		<a id='selfie' class="#6b3077">Selfie</a>
	</div>
</section>

	<div class='anim'>
		<div class='circle'></div>
		<p class='anim_banner'></p>
	</div>

<script type="text/javascript">

	$(window).on('load', function(){
	  $('.white').fadeOut(1000);
	  $('.loader').fadeOut(500);
	});

	$('.links a').click(function(e){
		e.preventDefault();
		var color = $(this).attr('class');
		var txt = $(this).html();
		var id = $(this).attr('id');
		$('.anim .circle, .anim .anim_banner').css('background-color', color);
		$('.anim .anim_banner').html(txt);
		$('.main_menu').fadeOut();
		animate(id);
	});

	function animate(id) {
		$.ajax({
		    url:'/home/' + id,
		        type:'GET',
		        success: function(data){
		        	var timer;

		        	timer = setTimeout(function(){
			        	$('header .menu').removeClass('open');
			        	$('.content').html($(data).find('.content').html()).fadeIn();
		        	}, 2000);
		        }
		});

		$('.anim .circle').fadeIn(700);
		$('.anim .circle').css('transform', 'scale(50)');

		setTimeout(function(){
			$('.anim_banner').fadeIn();
		}, 500);

		setTimeout(function(){
			$('.anim .circle').fadeOut(500);
			$('.anim .circle').css('transform', 'scale(1)');
			$('.anim_banner').addClass('banner');
		}, 1500);

		setTimeout(function(){
			$('.inner_page').show(300);
			setTimeout(function(){
				clearAnim();
			}, 2000);
		}, 1700);

	}


	function clearAnim() {
		$('.anim .circle, .anim_banner').css('background-color', 'transparent');
		$('.anim_banner').removeClass('banner');
		$('.anim_banner').html('').fadeOut();
	}


</script>
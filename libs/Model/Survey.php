<?php

namespace Model;

class Survey extends \Emagid\Core\Model {
    public static $tablename = "survey";

    public static $fields = [
        'question',
        'answer',
    ];
}
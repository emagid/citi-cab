<?php

namespace Model;

class Provider_Services extends \Emagid\Core\Model {
  
    static $tablename = "provider_services";
    public static $fields = ['provider_id','service_id'];

    static $relationships = [
  	[
  		'name'=>'provider',
  		'class_name' => '\Model\Provider',
  		'local'=>'provider_id',
  		'remote'=>'id',
      	'relationship_type' => 'one'
  	],
  	[
  		'name'=>'service',
  		'class_name' => '\Model\Service',
  		'local'=>'service_id',
  		'remote'=>'id',
      	'relationship_type' => 'one'
  	],
  ];

}
